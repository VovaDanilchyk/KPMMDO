window.addEventListener("DOMContentLoaded", (event) => {
    
    var selectRow = document.getElementById('Row');
    var selectColumn = document.getElementById('Column');
    var MatrixBlock = document.getElementById('Matrix');
    var FunctionBlock = document.getElementById('Function');
    var selectMinAndMax = `<select class="SelectedBlockOnFunction" name="BigMatrix[0][Sign]">
                        <option value="max">max</option>
                        <option value="min">min</option>
                    </select>`;
    var inputOfArray = '<input autocomplete="off" type="number" class="containerInput" required min="-999" max="999"';
    var selectOfArray = `
                        <option value="=">=</option> 
                        <option value="≤">≤</option>
                        <option value="≥">≥</option>
                </select>`;
    var widthRow = 4, heightColumn = 2;

    selectRow.addEventListener('change', function () {
        heightColumn = this.value;
        var BlockOfMatrix = '';

        for (var i = 1; i <= this.value; i++) {
            BlockOfMatrix += `<tr><td><div class="InLineBlocks">`;

            for (var j = 1; j <= widthRow - 1; j++) {
                BlockOfMatrix += `${inputOfArray} name="BigMatrix[${i}][${j - 1}]"> <div>x<sub>${j}</sub> +</div>`;
            };

            BlockOfMatrix += `${inputOfArray} name="BigMatrix[${i}][${widthRow - 1}]">
        <div>x<sub>${widthRow}</sub></div><select class="SelectedBlockOnParameters" name="BigMatrix[${i}][Sign]">
        ${selectOfArray} ${inputOfArray} name="BigMatrix[${i}][${widthRow}]" > </div></td></tr>`;
        };

        MatrixBlock.innerHTML = `<caption>Введіть значення системи обмежень:</caption>
    <td rowspan="9"><img src="img/sf5.png" height="`+ (15 + 60 * heightColumn) + `" width="15"></td> 
    ${BlockOfMatrix}`;
    });



    selectColumn.addEventListener('change', function () {
        var FunctionOfMatrix = '', BlockOfMatrix = '';
        widthRow = this.value;

        for (var i = 1; i <= heightColumn; i++) {
            BlockOfMatrix += `<tr><td><div class="InLineBlocks">`;

            for (var j = 1; j <= this.value - 1; j++) {
                BlockOfMatrix += `${inputOfArray} name="BigMatrix[${i}][${j - 1}]"> <div>x<sub>${j}</sub> +</div>`;
            };

            BlockOfMatrix += `${inputOfArray} name="BigMatrix[${i}][${widthRow - 1}]">
        <div>x<sub>${widthRow}</sub></div><select class="SelectedBlockOnParameters" name="BigMatrix[${i}][Sign]">
        ${selectOfArray} ${inputOfArray} name="BigMatrix[${i}][${widthRow}]" > </div></td></tr>`;
        };


        for (var i = 1; i <= this.value - 1; i++) {
            FunctionOfMatrix += `${inputOfArray} name="BigMatrix[0][${i - 1}]"> <div>x<sub>${i}</sub> +</div>`;
        };
        FunctionOfMatrix += `${inputOfArray} name="BigMatrix[0][${widthRow - 1}]"><div>x<sub>${widthRow}</sub> →</div>${selectMinAndMax}`;

        FunctionBlock.innerHTML = `
    <caption>Введіть значення цільової функції:</caption>
    <tr>
        <td>
            <div class="InLineBlocks">
                <div>F(x) =</div>
                ${FunctionOfMatrix}   
            </div>
        </td>
    </tr>
    `;

        MatrixBlock.innerHTML = `<caption>Введіть значення системи обмежень:</caption>
    <td rowspan="9"><img src="img/sf5.png" height="`+ (15 + 60 * heightColumn) + `" width="15"></td> 
    ${BlockOfMatrix}`;
    });
});
