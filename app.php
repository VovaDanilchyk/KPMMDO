<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles.css">
    <script src="scripts.js"></script>
    <title>Симплекс Метод</title>
</head>

<body>

    <div class="BigBlock">

        <div class="BlockOfContetnt">
            <div class="InLineBlocks DistanceBetweenBlocks">
                <div> Кількість обмежень: </div>
                <div class="ElementOnLeft">
                    <select class="SizeBlockOfMatriks" id="Row">
                        <option value="2" selected>2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                    </select>
                </div>
            </div>

            <div class="InLineBlocks DistanceBetweenBlocks">
                <div> Кількість змінних: </div>
                <div class="ElementOnLeft">
                    <select class="SizeBlockOfMatriks" id="Column">
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected>4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                    </select>
                </div>
            </div>
        </div>

        <div>
            <form method="post">
                <table style="margin: auto;" id="Function">
                    <caption>Введіть значення цільової функції:</caption>
                    <tr>
                        <td>
                            <div class="InLineBlocks">
                                <div>F(x) =</div>
                                <input autocomplete="off" name="BigMatrix[0][0]" type="number" lang="en-GB" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>1</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[0][1]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>2</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[0][2]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>3</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[0][3]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>4</sub> →</div>

                                <select class="SelectedBlockOnFunction" name="BigMatrix[0][Sign]">
                                    <option value="max">max</option>
                                    <option value="min">min</option>
                                </select>

                            </div>
                        </td>
                    </tr>
                </table>

                <table style="margin-left: auto; margin-right: auto" id="Matrix">

                    <caption>Введіть значення системи обмежень:</caption>
                    <td rowspan="9"><img src="img/sf5.png" height="135" width="15"></td>

                    <tr>
                        <td>
                            <div class="InLineBlocks">
                                <input autocomplete="off" name="BigMatrix[1][0]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>1</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[1][1]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>2</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[1][2]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>3</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[1][3]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>4</sub></div>

                                <select class="SelectedBlockOnParameters" name="BigMatrix[1][Sign]">
                                    <option value="=">=</option>
                                    <option value="≤">≤</option>
                                    <option value="≥">≥</option>
                                </select>

                                <input autocomplete="off" name="BigMatrix[1][4]" type="number" class="containerInput"
                                    required min="-999" max="999">
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="InLineBlocks">
                                <input autocomplete="off" name="BigMatrix[2][0]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>1</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[2][1]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>2</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[2][2]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>3</sub> +</div>

                                <input autocomplete="off" name="BigMatrix[2][3]" type="number" class="containerInput"
                                    required min="-999" max="999">
                                <div>x<sub>4</sub></div>

                                <select class="SelectedBlockOnParameters" name="BigMatrix[2][Sign]">
                                    <option value="=">=</option>
                                    <option value="≤">≤</option>
                                    <option value="≥">≥</option>
                                </select>

                                <input autocomplete="off" name="BigMatrix[2][4]" type="number" class="containerInput"
                                    required min="-999" max="999">
                            </div>
                        </td>
                    </tr>

                </table>
                <input type="submit" value="Обрахувати" class="ButtonSubmit" />
            </form>
        </div>
    </div>

    <?php
    $BigMatrix = !empty($_POST['BigMatrix']) ? $_POST['BigMatrix'] : null;

    if (!empty($BigMatrix)) {

        echo '<div class="BigBlock InLineBlocks"><div class="ElementOnRight"><table style="margin: auto;" id="Function">
        <tr><td><div class="InLineBlocks">F(x) = ';

        for ($i = 0; $i <= count($BigMatrix[0]) - 2; $i++) {
            if ($i <= count($BigMatrix[0]) - 3) {
                echo "{$BigMatrix[0][$i]}x<div><sub>" . ($i + 1) . "</sub></div> + ";
            } else {
                echo "{$BigMatrix[0][$i]}x<div><sub>" . ($i + 1) . "</sub></div> →";
                if ($BigMatrix[0]['Sign'] == "max")
                    echo " max ";
                else
                    echo " min ";
            }
        }
        echo "</div></td></tr></table>";

        echo '<table style="margin-left: auto; margin-right: auto" id="Matrix">
        <td rowspan="9"><img src="img/sf5.png" height="' . (15 + 55 * (count($BigMatrix) - 1)) . '" width="15"></td>';

        for ($i = 1; $i <= count($BigMatrix) - 1; $i++) {
            echo '<tr><td><div class="InLineBlocks">';

            for ($j = 0; $j <= count($BigMatrix[0]) - 2; $j++) {
                if ($j <= count($BigMatrix[0]) - 3) {
                    echo "{$BigMatrix[$i][$j]}x<div><sub>" . ($j + 1) . "</sub></div> + ";
                } else {
                    echo "{$BigMatrix[$i][$j]}x<div><sub>" . ($j + 1) . "</sub></div> ";
                    if ($BigMatrix[$i]['Sign'] == "=")
                        echo " = ";
                    elseif ($BigMatrix[$i]['Sign'] == "≤") {
                        echo " ≤ ";
                    } else {
                        echo " ≥ ";
                    }
                    echo "{$BigMatrix[$i][$j + 1]} ";
                }
            }
            echo "</div></td></tr>";
        }
        echo "</table></div>";
        echo '<div class="ElementOnCenter"><img src="img/arrow.png" height="50" width="auto"></div>';

        $counterOfVariables = 0;
        $array = array();// 0 => array(), 
        $array[0][0] = "C";
        $array[0][1] = "-";

        echo '<div class="ElementOnLeft"><table style="margin: auto;" id="Function">
        <tr><td><div class="InLineBlocks">F(x) = ';

        for ($i = 0; $i <= count($BigMatrix[0]) - 2; $i++) {
            if ($i <= count($BigMatrix[0]) - 3) {

                echo "{$BigMatrix[0][$i]}x<div><sub>" . ($i + 1) . "</sub></div> + ";

                array_push($array[0], $BigMatrix[0][$i]);

            } else {

                $counterOfVariables = $i + 2;
                echo "{$BigMatrix[0][$i]}x<div><sub>" . ($i + 1) . "</sub></div>";
                array_push($array[0], $BigMatrix[0][$i]);

                for ($j = 1; $j <= count($BigMatrix) - 1; $j++) {

                    if ($BigMatrix[$j]['Sign'] == '=') {
                        if ($BigMatrix[0]['Sign'] == "max") {
                            echo " - ";
                            array_push($array[0], '-M');
                        } else {
                            echo " + ";
                            array_push($array[0], 'M');
                        }
                        $array[$j + 1][0] = "x<sub>$counterOfVariables</sub>";
                        array_push($array[$j + 1], end($BigMatrix[$j]));
                        for ($e = 0; $e < count($BigMatrix[$j]) - 2; $e++) {
                            array_push($array[$j + 1], $BigMatrix[$j][$e]);
                        }

                        echo "Mx<div><sub>$counterOfVariables</sub></div>"; // =
    
                    } elseif ($BigMatrix[$j]['Sign'] == '≤') {

                        echo " + 0x<div><sub>$counterOfVariables</sub></div>"; // ≤
                        array_push($array[0], '0');
                        $array[$j + 1][0] = "x<sub>$counterOfVariables</sub>";
                        array_push($array[$j + 1], end($BigMatrix[$j]));
                        for ($e = 0; $e < count($BigMatrix[$j]) - 2; $e++) {
                            array_push($array[$j + 1], $BigMatrix[$j][$e]);
                        }

                    } else {

                        echo " + 0x<div><sub>$counterOfVariables</sub></div>";
                        array_push($array[0], '0');
                        $counterOfVariables++;

                        if ($BigMatrix[0]['Sign'] == "max") {
                            echo " - ";
                            array_push($array[0], '-M');
                        } else {
                            echo " + ";
                            array_push($array[0], 'M');
                        }
                        $array[$j + 1][0] = "x<sub>$counterOfVariables</sub>";
                        array_push($array[$j + 1], end($BigMatrix[$j]));
                        for ($e = 0; $e < count($BigMatrix[$j]) - 2; $e++) {
                            array_push($array[$j + 1], $BigMatrix[$j][$e]);
                        }

                        echo "Mx<div><sub>$counterOfVariables</sub></div>"; // ≥ 
    
                    }

                    $counterOfVariables++;

                }

                $counterOfVariables = $i + 2;
                echo " →";

                if ($BigMatrix[0]['Sign'] == "max")
                    echo " max ";
                else
                    echo " min ";
            }
        }
        echo "</div></td></tr></table>";

        $array[1][0] = "B";
        for ($i = 0; $i < count($array[0]) - 1; $i++) {
            array_push($array[1], "x<sub>$i</sub>");
        }

        $array[count($array)][0] = " 𐤃 ";

        for ($i = 2; $i < count($array) - 1; $i++) {
            for ($j = count($BigMatrix[1]) - 1; $j < count($array[0]) - 1; $j++) {
                if ($array[$i][0] == $array[1][$j + 1]) {
                    if ($BigMatrix[$i - 1]['Sign'] == "=") {
                        $array[$i][$j + 1] = "1";
                    } elseif ($BigMatrix[$i - 1]['Sign'] == "≤") {
                        $array[$i][$j + 1] = "1";
                    } else {
                        $array[$i][$j + 1] = "1";
                        $array[$i][$j] = "-1";
                    }
                } else {
                    array_push($array[$i], "0");
                }
            }
        }

        echo '<table style="margin-left: auto; margin-right: auto" id="Matrix">
        <td rowspan="9"><img src="img/sf5.png" height="' . (15 + 55 * (count($BigMatrix) - 1)) . '" width="15"></td>';

        for ($i = 1; $i <= count($BigMatrix) - 1; $i++) {
            echo '<tr><td><div class="InLineBlocks">';
            for ($j = 0; $j <= count($BigMatrix[0]) - 2; $j++) {
                if ($j <= count($BigMatrix[0]) - 3) {
                    echo "{$BigMatrix[$i][$j]}x<div><sub>" . ($j + 1) . "</sub></div> + ";
                } else {
                    echo "{$BigMatrix[$i][$j]}x<div><sub>" . ($j + 1) . "</sub></div> ";
                    if ($BigMatrix[$i]['Sign'] == '=') {
                        echo " + x<div><sub>$counterOfVariables</sub></div> = ";
                    } elseif ($BigMatrix[$i]['Sign'] == '≤') {
                        echo " + x<div><sub>$counterOfVariables</sub></div> = "; // ≤
                    } else {
                        echo " - x<div><sub>$counterOfVariables</sub></div>";
                        $counterOfVariables++;
                        echo " + x<div><sub>$counterOfVariables</sub></div> = "; // ≥
                    }
                    $counterOfVariables++;
                    echo "{$BigMatrix[$i][$j + 1]} ";
                }
            }
            echo "</div></td></tr>";
        }
        echo "</table></div></div>";

        $array['Odds'][0] = '';
        $array['Odds'][1] = '';

        for ($i = 2; $i < count($array) - 1; $i++) {

            echo '<tr>';
            if ($i > 1 && $i < count($array) - 2) {
                for ($j = 1; $j <= count($array[0]) - 1; $j++) {
                    if ($array[$i][0] == $array[1][$j]) {
                        array_push($array['Odds'], $array[0][$j]);
                        break;
                    }
                }
            } else {
                array_push($array['Odds'], '');
            }
        }


        for ($i = 1; $i < count($array[0]); $i++) {
            $sumElementWithM = 0;
            $sumElement = 0;
            $res = '';
            for ($j = 2; $j < count($array) - 2; $j++) {
                if ($array['Odds'][$j] == '0') {
                    continue;
                } elseif ($array['Odds'][$j] == 'M') {
                    $sumElementWithM += round(floatval($array[$j][$i]), 2);
                } elseif ($array['Odds'][$j] == '-M') {
                    $sumElementWithM -= round(floatval($array[$j][$i]), 2);
                } else {
                    $sumElement += round(floatval($array[$j][$i]) * $array['Odds'][$j], 2);
                }
            }

            if (is_numeric($array[0][$i])) {
                $sumElement -= floatval($array[0][$i]);
            } elseif ($array[0][$i] == 'M') {
                $sumElementWithM -= 1;
            } elseif ($array[0][$i] == '-M') {
                $sumElementWithM += 1;
            }

            if ($sumElementWithM != 0)
                $res = $sumElementWithM . "M";

            if ($sumElement != 0) {
                if ($sumElement >= 0) {
                    if ($sumElementWithM != 0)
                        $res .= "+" . $sumElement;
                    else
                        $res = $sumElement;

                } else {
                    $res .= $sumElement;
                }
            }

            if ($res == '')
                $res = 0;

            array_push($array[count($array) - 2], $res);
        }


        echo '<div class="BigBlock">';

        for ($i = 0; $i <= 7; $i++) {

            if ($i == 7) {
                echo "<div class='TextOnCenter'> Відповіді немає <div>";
                break;
            }
            echo "<div class='TextOnCenter'> Cимплекст таблиця №" . ($i + 1) . "</div>";
            $chekerOfAnswer = drawTheTable($array, $BigMatrix);
            if ($chekerOfAnswer == 0) {

                echo "<div class='TextOnCenter'>F =  {$array[count($array) - 2][1]}</div><div class='TextOnCenter'>";
                for ($i = 2; $i <= count($BigMatrix[1]) - 1; $i++) {

                    $findElement = 0;

                    for ($j = 0; $j <= count($BigMatrix); $j++) {
                        if ($array[1][$i] == $array[$j][0]) {
                            echo "{$array[1][$i]} = " . round($array[$j][1], 2) . "; ";
                            $findElement = 1;
                        }
                    }

                    if ($findElement == 0) {
                        echo "{$array[1][$i]} =  0; ";
                    }
                }

                break;
            }

            $array = countArray($array, $BigMatrix);
            if ($array == 0) {
                echo "<div class='TextOnCenter'> Відповіді немає <div>";
                break;
            }
        }

        echo '</div></div>';

    }

    function countArray($array, $BigMatrix)
    {
        $maxIndexColumn = FindMaxColumn($array, $BigMatrix);
        $minIndexRow = FindMinRow($array, $BigMatrix);
        if ($minIndexRow == 0)
            return 0;

        for ($i = 2; $i < count($array) - 2; $i++) {
            $arrayRes;
            for ($j = 1; $j < count($array[0]); $j++) {
                if ($minIndexRow == $i && $maxIndexColumn == $j) {

                    $arrayRes[$i][0] = $array[1][$j];
                    $arrayRes["Odds"][$j] = $array[0][$j];
                    $arrayRes[$i][$j] = 1;

                } elseif ($minIndexRow == $i) {
                    $res = round($array[$i][$j] / $array[$minIndexRow][$maxIndexColumn], 6);
                    $arrayRes[$i][$j] = $res;
                } elseif ($maxIndexColumn == $j) {
                    $arrayRes[$i][$j] = 0;
                } else {
                    $res = round($array[$i][$j] - ($array[$i][$maxIndexColumn] *
                        $array[$minIndexRow][$j]) / $array[$minIndexRow][$maxIndexColumn], 6);
                    $arrayRes[$i][$j] = $res;
                }
            }
        }

        for ($i = 2; $i < count($array) - 2; $i++) {
            for ($j = 1; $j < count($array[0]); $j++) {
                if ($minIndexRow == $i && $maxIndexColumn == $j) {
                    $array[$i][0] = $arrayRes[$i][0];
                    $array["Odds"][$i] = $arrayRes["Odds"][$j];
                }
                $array[$i][$j] = $arrayRes[$i][$j];
            }
        }

        unset($array[count($array) - 2]);
        $array[count($array) - 1][0] = " 𐤃 ";

        for ($i = 1; $i < count($array[0]); $i++) {
            $sumElementWithM = 0;
            $sumElement = 0;
            $res = '';
            for ($j = 2; $j < count($array) - 2; $j++) {
                if ($array['Odds'][$j] == '0') {
                    continue;
                } elseif ($array['Odds'][$j] == 'M') {
                    $sumElementWithM += round(floatval($array[$j][$i]), 2);
                } elseif ($array['Odds'][$j] == '-M') {
                    $sumElementWithM -= round(floatval($array[$j][$i]), 2);
                } else {
                    $sumElement += round(floatval($array[$j][$i]) * $array['Odds'][$j], 2);
                }
            }

            if (is_numeric($array[0][$i])) {
                $sumElement -= floatval($array[0][$i]);
            } elseif ($array[0][$i] == 'M') {
                $sumElementWithM -= 1;
            } elseif ($array[0][$i] == '-M') {
                $sumElementWithM += 1;
            }

            if ($sumElementWithM != 0)
                $res = $sumElementWithM . "M";

            if ($sumElement != 0) {
                if ($sumElement >= 0) {
                    if ($sumElementWithM != 0)
                        $res .= "+" . $sumElement;
                    else
                        $res = $sumElement;

                } else {
                    $res .= $sumElement;
                }
            }

            if ($res == '')
                $res = 0;

            array_push($array[count($array) - 2], $res);
        }
        return $array;
    }

    function drawTheTable($array, $BigMatrix)
    {
        $maxIndexColumn = FindMaxColumn($array, $BigMatrix);
        $minIndexRow = 0;
        if ($maxIndexColumn != 0) {
            $minIndexRow = FindMinRow($array, $BigMatrix);
        }
        echo '<table class="TableOnCenter">';

        for ($i = 0; $i < count($array) - 1; $i++) {

            echo '<tr>';
            if ($i > 1 && $i < count($array) - 1) {
                echo '<td style="background-color: rgb(219, 216, 216); border: 0px;">';
                for ($j = 1; $j <= count($array[0]) - 1; $j++) {
                    if ($array[$i][0] == $array[1][$j]) {
                        echo " {$array[0][$j]} </td>";
                        break;
                    }
                }
            } else {
                echo '<td style="background-color: rgb(219, 216, 216); border: 0px;"></td>';
            }
            if ($maxIndexColumn != 0) {
                for ($j = 0; $j < count($array[$i]); $j++) {
                    if (is_float($array[$i][$j])) {
                        $element = round(floatval($array[$i][$j]), 2);
                    } else {
                        $element = $array[$i][$j];
                    }

                    if ($minIndexRow == 0) {
                        echo "<td>  $element </td>";
                    } else {
                        if ($minIndexRow == $i && $maxIndexColumn == $j) {
                            echo "<td style='background-color: rgb(106, 168, 79);'>  $element </td>";
                        } elseif ($minIndexRow == $i || $maxIndexColumn == $j) {
                            echo "<td style='background-color: rgb(182, 215, 168);'>  $element </td>";
                        } else {
                            echo "<td>  $element </td>";
                        }
                    }

                }
            } else {
                for ($j = 0; $j < count($array[$i]); $j++) {
                    if (is_float($array[$i][$j])) {
                        $element = round(floatval($array[$i][$j]), 2);
                    } else {
                        $element = $array[$i][$j];
                    }
                    echo "<td> $element </td>";
                }
            }
            echo "</tr>";
        }
        echo "</table>";
        if ($maxIndexColumn != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    function FindMaxColumn($array, $BigMatrix)
    {
        $maxElement = 0;
        $maxIndex = 0;
        for ($i = 2; $i < count($array[0]); $i++) {
            $element = explode("M", $array[count($array) - 2][$i])[0];
            if ($BigMatrix[0]['Sign'] == 'max') {
                if ($maxElement >= $element && $element != 0) {
                    $maxElement = $element;
                    $maxIndex = $i;
                }

            } else {
                if ($maxElement <= $element && $element != 0) {
                    $maxElement = $element;
                    $maxIndex = $i;
                }
            }
        }
        return $maxIndex;
    }

    function FindMinRow($array, $BigMatrix)
    {
        $maxIndexColumn = FindMaxColumn($array, $BigMatrix);
        $minElementRow = 9999;
        $minIndexRow = 0;
        for ($i = 2; $i < count($array) - 2; $i++) {
            if ($array[$i][1] > 0 && $array[$i][$maxIndexColumn] > 0) {
                if ($minElementRow > $array[$i][1] / floatval($array[$i][$maxIndexColumn])) {
                    $minElementRow = $array[$i][1] / floatval($array[$i][$maxIndexColumn]);
                    $minIndexRow = $i;
                }
            }
        }
        return $minIndexRow;
    }

    ?>
</body>

</html>